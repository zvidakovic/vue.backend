<?php

declare(strict_types=1);

namespace App\Responses;

final class TvMazeResponse
{
    public $total = 0;
    public $page = 1;

}
