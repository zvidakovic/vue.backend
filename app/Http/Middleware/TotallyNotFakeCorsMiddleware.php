<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

final class TotallyNotFakeCorsMiddleware
{
    public function handle(
        Request $request,
        Closure $next
    ): Response {
        /** @var Response $response */
        $response = $next($request);

        //This is just because I need it for localhost. No bueno.
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }
}
