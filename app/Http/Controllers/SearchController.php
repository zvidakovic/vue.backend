<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\TvMazeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use JsonException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

final class SearchController extends Controller
{
    /**
     * @var TvMazeService
     */
    private $tvMazeService;

    public function __construct(TvMazeService $tvMazeService)
    {
        $this->tvMazeService = $tvMazeService;
    }

    public function search(Request $request): Response
    {
        $query = (string)trim($request->get('q', ''));
        if (empty($query)) {
            abort(422, 'Missing query parameter.');
        }

        $page = (int)$request->get('page', 1);
        if ($page < 1 || $page > 10) {
            $page = 1;
        }

        $itemsPerPage = (int)$request->get('itemsPerPage', 3);
        if ($itemsPerPage < 1 || $itemsPerPage > 6) {
            $itemsPerPage = 3;
        }

        $data = [];
        try {
            $data = $this->tvMazeService->queryWithoutFuzziness($query);
        } catch (JsonException $error) {
            error_log($error->getMessage());
            abort(500, 'Could not decode external service response.');
        } catch (Throwable $error) {
            error_log($error->getMessage());
            abort(500, 'External service not available.');
        }

        return new JsonResponse(array_splice($data, ($page - 1) * $itemsPerPage, $itemsPerPage));
    }
}
