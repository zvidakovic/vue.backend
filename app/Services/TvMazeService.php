<?php

declare(strict_types=1);

namespace App\Services;

use GuzzleHttp\Client;
use JsonException;

final class TvMazeService
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Query API and filter/match exact title.
     * @param string $query
     * @return array
     * @throws JsonException
     */
    public function queryWithoutFuzziness(string $query): array
    {
        return array_filter(
            $this->query($query),
            static function (array $show) use ($query) : bool {
                $name = (string)($show['show']['name'] ?? '');
                return mb_stripos($name, $query) !== false;
            }
        );
    }

    private function query(string $query, int $page = 1): array
    {
        $response = $this->client->get(
            '/search/shows',
            [
                'query' => [
                    'q' => $query,
                    'page' => $page,
                ],
            ]
        );
        return json_decode((string)$response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
    }
}
