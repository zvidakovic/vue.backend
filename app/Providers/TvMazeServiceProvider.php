<?php

declare(strict_types=1);

namespace App\Providers;

use App\Services\TvMazeService;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

final class TvMazeServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(
            TvMazeService::class,
            static function (): TvMazeService {
                return new TvMazeService(
                    new Client(['base_uri' => 'https://api.tvmaze.com', 'timeout' => 10])
                );
            }
        );
    }
}
