# PROJECT SETUP

This project assumes you have `docker` correctly setup on your
local machine.

```
docker-compose up
docker-compose exec php composer i -n
```

# EXAMPLE

[http://127.0.0.1:9090/?q=deadwood](http://127.0.0.1:9090/?q=deadwood)
